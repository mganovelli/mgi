<?php

/**
 * @file
 * Contains Drupal\mgi\Controller\CustomTestPage.
 */

namespace Drupal\mgi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\user\UserInterface;

/**
 * Class CustomTestPage.
 *
 * @package Drupal\mgi\Controller
 */
class CustomTestPage extends ControllerBase {
  
  public function grazie() {
    return array(
      '#type' => 'markup',
      '#markup' => '<div>Grazie per aver usato il form!</div>'
    );
  }

  public function parametri(int $par1, int $par2) {
    return array(
      '#type' => 'markup',
      '#markup' => 'Hai inserito: ' . SafeMarkup::checkPlain($par1) . ' ' . SafeMarkup::checkPlain($par2),
    );
  }

  public function utente(UserInterface $user) {
    return array(
      '#type' => 'markup',
      '#markup' => 'Utente: ' . $user->uuid,
    );
  }

}
