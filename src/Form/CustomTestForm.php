<?php

/**
 * @file
 * Contains Drupal\mgi\Form\CustomTestForm.
 */

namespace Drupal\mgi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class CustomTestForm.
 *
 * @package Drupal\mgi\Form\CustomTestForm
 */
class CustomTestForm extends FormBase {
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mgi_form_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['uno'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => 'Campo uno',
    );
    
    $form['due'] = array(
      '#type' => 'select',
      '#options' => array(1 => 'Uno', 2 => 'Due', 3 => 'Tre'),
      '#title' => 'Campo due',
    );
    
    $form['tre'] = array(
      '#type' => 'radios',
      '#options' => array(1 => 'Uno', 2 => 'Due', 3 => 'Tre'),
      '#title' => 'Campo tre',
    );
    
    $form['redirect'] = array(
      '#type' => 'select',
      '#title' => 'Seleziona azione dopo submit',
      '#options' => array(
        1 => 'Redireziona alla pagina di stampa dei campi',
        2 => 'Redireziona alla home',
        3 => 'Ricostruisce il form',
      ),
      '#required' => TRUE,
    );

    if ($form_state->isSubmitted()) {
      $form['risultati'] = array(
        '#theme' => 'item_list',
        '#items' => array(
          'uno: ' . $form_state->getValue('uno'), 
          'due: ' . $form_state->getValue('due'),  
          'tre: ' . $form_state->getValue('tre'),
        ),
        '#type' => 'ol',
      );
    }
    
    $form['action'] = array('#type' => 'actions');
    
    $form['action']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Invia dati',
    );
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
   
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    if ($form_state->getValue('redirect') == 1) {
      $dest = Url::fromRoute('mgi.parametri', 
        array(
          'par1' => $form_state->getValue('due'), 
          'par2' => $form_state->getValue('tre')
        )
      );
      $form_state->setRedirectUrl($dest);
    } else if ($form_state->getValue('redirect') == 2) {
      $dest = Url::fromRoute('mgi.pagina');
      $form_state->setRedirectUrl($dest);
    } else if ($form_state->getValue('redirect') == 3) {
      $form_state->setRebuild();
    }
    
  }

}
